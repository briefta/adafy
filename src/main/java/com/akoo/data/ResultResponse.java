package com.akoo.data;

import java.util.List;

public class ResultResponse {
	private String response;
	private List<Integer> intArray;
	
	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public List<Integer> getIntArray() {
		return intArray;
	}

	public void setIntArray(List<Integer> intArray) {
		this.intArray = intArray;
	}

}
